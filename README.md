# simple greenhouse automation

This is a Arduino IDE compatible program for a simple greenhouse automation. It should run on ATMega-Based Arduino (UNO, Nano, ...) as well as on ESP8266 based Boards.
The main focus is on ESP8266 but the we are still in the development and testing phase!

## License
GNU General Public License v3.0

## Project status
Two person educational project.

