/**
 * Simple greenhouse automation
 *
 * @license GNU GPLv3
 *
 * Copyright (c) 2021 bastelix
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * Wichtig: Es dürfen im Moment nicht mehr als zwei DS18B20 Sensoren am 1-Wire-Bus
 * betrieben werden, da sonst die Außen-/Innenfühler-Erkennung nicht mehr
 * zuverlässig funktioniert
 */

/*
 * DEBUG compiler switch
 * Wenn 1 werden über die Serielle Schnittstelle DEBUG-Nachrichten ausgegeben
 * Für den Produktivbetrieb dann auf 0 setzten um Speicher und Strom zu sparen
 */
#define DEBUG 1

/*
 * Deep sleep benötigt eine Verbindung zwischen GPIO16 und dem RESET Pin
 * Wenn deep sleep verwendet werden soll hier 1 ansonsten 0
 *
 * Mit der geplanten beschaltung wird der deep sleep nicht funktionieren!
 */
#define USE_DEEP_SLEEP 0

/*
 * Light sleep wird verwendet wenn deep sleep nicht verwendet wird
 * soll gar kein speep modus verwendet werden hier auch 0 statt 1
 */
#define USE_LIGHT_SLEEP 1

/*
 * Wenn Relais verwendet werden, schalten diese meistens mit inverser logik,
 * d.h. das Relais zieht an, wenn LOW anliegt und fällt ab wenn HIGH anliegt.
 * Wenn SWITCH_INVERSE_LOGIC auf 1 gesetzt wird schalten die Relais Pins invers
 * bei 0 normal.
 */
#define SWITCH_INVERSE_LOGIC 1

/*
 * Abhängit von SWITCH_INVERSE_LOGIC werden die SWITCH_OFF und SWITCH_ON macros definiert
 */
#if SWITCH_INVERSE_LOGIC
  #define SWITCH_OFF HIGH
  #define SWITCH_ON  LOW
#else
  #define SWITCH_OFF HIGH
  #define SWITCH_ON  LOW
#endif

/*
 * Definition der Pins für die jeweiligen Sensoren und Aktuator
 */
// Pin für den Feuchtigkeitssensor
static const uint8_t MOISTURE_SENSOR = A0; // = A0
// Pin für den 1-Wire-Bus (DS18B20 Temperaturfühler)
static const uint8_t ONE_WIRE_BUS    = 12; // = D6 / GPIO12
// Pin für das Relais des Lüfters
static const uint8_t FAN_PIN         = 13; // = D7 / GPIO13
// Pin für das Relais der Wasserversorgung (Pumpe/Magnetventil/Alarmleuchte für Gartenzwerge/...)
static const uint8_t WATER_PIN       = 15; // = D8 / GPIO15

/*
 * Die indizes für die Temperaturfühler auf dem 1-Wire-Bus
 */
// Index des Temepraturfühlers außen
static const uint8_t TEMP_OUTDOOR    = 0;
// Index des Temepraturfühlers innen
static const uint8_t TEMP_INDOOR    = 1;

/*
 * Multiplikator für die Umrechnung in ms, zum testen wird der faktor einfach kleiner gesetzt.
 * Für den produktivbetrieb muss der Wert auf 1000 stehen, damit die Sekunden korrekt in
 * Millisekunden umgerechnet werden.
 */
static const unsigned long MULTIPLIER = 1000; // default 1000

/*
 * Die dauer des Sleep-Phase in ms
 */
static const unsigned long SLEEP_TIME = 10000;

/*
 * Definition der Grenzwerte und Laufzeiten
 */
// Mindesttemperatur Außen, ab der die Belüftung des GWH erfolgen soll
static const float OUTDOOR_MIN_TEMP = 20.0f;
// Temperaturdifferenz (minimum) ab der die Belüftung eingeschaltet wird
static const float MIN_DIFF_TEMP    = 15.0f;
// Zeit die der Lüfter laufen soll, in millisekunden
static const unsigned long FAN_DURATION = 10 * 60 * MULTIPLIER; // 10min * 60 * 1000 -> 600000ms
// Zeit die der Lüfter pausieren soll
static const unsigned long FAN_PAUSE = 60 * 60 * MULTIPLIER;

// Wert ab dem bei der Feuchtigkeitsmessung die Pumpe eingeschaltet werden soll
static const int MOISTURE_MIN_LEVEL = 500;
// Zeit die die Pumpe laufen soll, in millisekunden
static const unsigned long WATER_DURATION = 5 * 60 * MULTIPLIER; //  5min * 60 * 1000 -> 300000ms
// Zeit welche die Bewässerung pausieren sol, in millisekunden
static const unsigned long WATER_PAUSE = 60 * 60 * MULTIPLIER;

/*
 * Der Lüfter und die Bewässerung haben aktuell drei Zustände, Aus, Ein und Pause.
 * Pause ist der Zustand, wenn der Kanal auf Ein war, die Zeit dafür abgelaufen ist aber es
 * noch gewartet werden soll bis er wieder vom Zustand Aus auf Ein wechseln darf. Quasi eine
 * zeitbasierte Hysterese.
 *
 * Der Name ist nicht besonders glücklich gewählt, mir fällt aber auch nichts besseres ein.
 */
enum switch_state_t {
  SWITCH_STATE_OFF,
  SWITCH_STATE_ON,
  SWITCH_STATE_PAUSED
};

/*
 * Importieren der benötigten Bibliotheken
 */
#include <OneWire.h>
#include <DallasTemperature.h> // https://github.com/milesburton/Arduino-Temperature-Control-Library

#if defined(ESP8266)
  // schreibt eine info in die compiler ausgabe
  #pragma message ( "using ESP8266 as compilation target" )

  #include <ESP8266WiFi.h>
  extern "C" {
    #include "user_interface.h"
  }
#endif

/*
 * Deklaration der 'golbalen' Variablen
 */

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

unsigned long fanTime = 0;
// FIXME klappt das so mit den zeiten
unsigned long fanPauseTime = 0;
switch_state_t fanState = SWITCH_STATE_OFF;

unsigned long waterTime = 0;
unsigned long waterPauseTime = 0;
switch_state_t waterState = SWITCH_STATE_OFF;

/**
 * Initialisierne des µC, der Pin-Beschaltung und der Module
 */
void setup() {
  // Pins in einen definierten Zustand bringen, externe Pull-Up/Down schaden bestimmt nicht
  pinMode(FAN_PIN, OUTPUT);
  digitalWrite(FAN_PIN, SWITCH_OFF);

  pinMode(WATER_PIN, OUTPUT);
  digitalWrite(WATER_PIN, SWITCH_OFF);

  // Nur verwenden, wenn das Programm für einen ESP8266 compiliert wird
  #if defined(ESP8266)
    // WLAN abschalten heißt strom Sparen
    WiFi.mode(WIFI_OFF);
    WiFi.forceSleepBegin();
    delay(1);

    #if USE_LIGHT_SLEEP
      wifi_set_sleep_type(LIGHT_SLEEP_T);
      delay(1);
    #endif

  #endif

  // Serielle Ausgabe initialisieren, falls wir im DEBUG-Modus compilieren
  #if DEBUG
    Serial.begin(115200);
    sensors.begin();
    delay(50);

    Serial.println(F("\n=======================================\n"));
    Serial.println(F("Simple greenhouse automation\n"));

    #if USE_DEEP_SLEEP
      Serial.print(F("Using deep sleep with "));
      Serial.print(SLEEP_TIME);
      Serial.print(F("µs"));
    #elif USE_LIGHT_SLEEP
      Serial.print(F("Using deep sleep with "));
      Serial.print(SLEEP_TIME);
      Serial.print(F("µs"));
    #endif

    Serial.print(F("FAN_DURATION "));
    Serial.print(FAN_DURATION / 1000);
    Serial.println(F("s"));

    Serial.print(F("FAN_PAUSE "));
    Serial.print(FAN_PAUSE / 1000);
    Serial.println(F("s"));

    Serial.print(F("WATER_DURATION "));
    Serial.print(WATER_DURATION / 1000);
    Serial.println(F("s"));

    Serial.print(F("WATER_PAUSE "));
    Serial.print(WATER_PAUSE / 1000);
    Serial.println(F("s"));

    Serial.println(F("=======================================\n"));
  #endif
}

/**
 * Die main loop
 */
void loop() {
  checkTemperature();

  checkMoisture();

  doSleep();
}

/**
 * Prüft den Feuchtigkeitssensor und schaltet die Bewässerung.
 */
void checkMoisture() {
  #if DEBUG
    Serial.println("=== Prüfe Bodenfeuchte");
  #endif

  // Datasheet https://www.sigmaelectronica.net/wp-content/uploads/2018/04/sen0193-humedad-de-suelos.pdf
  // TODO error compensation
  int moisture = 0;

  // wert mehrfach auslesen, um mögliche Messfehler zu kompensieren
  for (uint8_t i = 0; i < 3; ++i) {
     moisture += analogRead(MOISTURE_SENSOR);
     // 100ms warten bis wir nochmal messen
     delay(100);
  }
  // Mittelwert bestimmen
  moisture = moisture / 3;

  #if DEBUG
    Serial.print(F("Feuchtigkeit mittelwert: "));
    Serial.println(moisture);
  #endif

  if (moisture > MOISTURE_MIN_LEVEL) {
    waterOn();
  } else {
    waterOff();
  }

  if (waterState == SWITCH_STATE_PAUSED && millis() - waterPauseTime > WATER_PAUSE) {
    #if DEBUG
      Serial.println(F("### Reset water state"));
    #endif

    waterState = SWITCH_STATE_OFF;
  }
}

/**
 * Schaltet die Bewässerung ein, wenn der Zustand aktuell OFF ist.
 */
void waterOn() {
  // TODO was ist wenn die Pumpe 5 Minuten geloffen ist und immer noch die bedingungen für wateren erfüllt sind?
  if (waterState == SWITCH_STATE_OFF) {
    #if DEBUG
      Serial.println(F("*** Water on"));
    #endif

    waterState = SWITCH_STATE_ON;
    waterTime = millis();
    digitalWrite(WATER_PIN, SWITCH_ON);
  }
}

/**
 * Schaltet die Bewässerung aus, setzt den Zustand auf PAUSED
 */
void waterOff() {
  if(waterState == SWITCH_STATE_ON && millis() - waterTime > WATER_DURATION) {
    #if DEBUG
      Serial.println(F("*** Water off"));
    #endif

    waterState = SWITCH_STATE_PAUSED;
    waterPauseTime = millis();
    digitalWrite(WATER_PIN, SWITCH_OFF);
  }
}

/**
 * Prüft die Temperatur-Sensoren und schaltet den Lüfter.
 */
void checkTemperature() {
  #if DEBUG
    Serial.println(F("=== Prüfe Temperatur"));
  #endif

  // Alle DS18* Sensoren auslesen
  sensors.requestTemperatures();

  // Anhand der Indizes die jeweiligen Messerten für innen und außen zuweisen
  float outdoor = sensors.getTempCByIndex(TEMP_OUTDOOR);
  float indoor = sensors.getTempCByIndex(TEMP_INDOOR);

  #if DEBUG
    Serial.print(F("Temperatur außen: "));
    Serial.print(outdoor);
    Serial.println(F("ºC"));

    Serial.print(F("Temperatur innen: "));
    Serial.print(indoor);
    Serial.println(F("ºC"));
  #endif

  // Wir prüfen hier also ob die gemessenen temperaturen zwischen 84°C und 86°C liegen,
  // da bei float eine ist-gleich Prüfung wegen möglicher Rundungsfehler nicht zu
  // empfehlen ist. Der DS18B20 liefert +85°C wenn er noch nicht richtig initialisiert
  // ist, das kann auch ein Fehlerzustand im laufenden Betrieb sein.
  // "The power-on reset value of the temperature register is +85°C"
  // (vgl. https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf Seite 6)
  if ((outdoor > 84.0f && outdoor < 86.0f) || (indoor > 84.0f && indoor < 86.0f)) {
    // Einer der Messwerte ist mist, also brechen wir hier ab und warten auf den nächsten
    // Messzyklus. Alternativ könne man auch ein paar (m)s warten und nochmal messen.
    return;
  }

  // REQUIREMENT: Ich möchte im Gewächshaus, ab 20C Außentemperatur, Frischluft haben wenn
  // die Temperatur von innen und außen mehr als 15C unterschied beträgt. Dann soll ein
  // Lüfter für 10 Minuten laufen.
  if (outdoor >= OUTDOOR_MIN_TEMP && indoor - outdoor >= MIN_DIFF_TEMP) {
    fanOn();
  } else {
    fanOff();
  }

  if (fanState == SWITCH_STATE_PAUSED &&  millis() - fanPauseTime > FAN_PAUSE) {
    #if DEBUG
      Serial.println(F("### Reset fan state"));
    #endif

    fanState = SWITCH_STATE_OFF;
  }
}

/**
 * Schaltet den Lüfter ein wenn der Status OFF ist.
 */
void fanOn() {
  // FIXME overflow von millis ist ein Problem
  if (fanState == SWITCH_STATE_OFF) {
    #if DEBUG
      Serial.println(F("*** Fan on"));
    #endif

    fanState = SWITCH_STATE_ON;
    fanTime = millis();
    digitalWrite(FAN_PIN, SWITCH_ON);
  }
}

/**
 * Schaltet den Lüfter aus, setzt den Status auf PAUSED.
 */
void fanOff() {
  if(fanState == SWITCH_STATE_ON && millis() - fanTime > FAN_DURATION) {
    #if DEBUG
      Serial.println(F("*** Fan off"));
    #endif

    fanState = SWITCH_STATE_PAUSED;
    fanPauseTime = millis();
    digitalWrite(FAN_PIN, SWITCH_OFF);
  }
}

/**
 * Versetzt den µC für eine gewisse Zeit in den schlafmodus um Strom zu sparen.
 */
void doSleep() {
  #if DEBUG
    // Der sleep mode könnte probleme mit der Seriellen schnittstelle machen, darauf lassen wir
    // es vorerst mal ankommen. Wenn die serielle Ausgabe nach dem Aufwachen nicht mehr geht
    // müsste man diese Stelle anpassen
    Serial.println("Enter sleep mode");
    //delay(10000);
  #endif

  #if defined(ESP8266)
    #if USE_DEEP_SLEEP
      #if DEBUG
        Serial.println("deep sleep");
      #endif

      // requires GPIO16 connected to RESET pin
      // void deepSleep(uint64_t time_us, WakeupMode mode)
      // Wir mulitplizeiren SLEEP_TIME mit 1000 um von den ms, welche light sleep verwendet
      // auf die µs welche deep sleep verwendet zu kommen ohne die Konfiguration abhängig
      // vom verwendeten sleep mode zu machen.
      ESP.deepSleep(SLEEP_TIME * 1000, RF_DISABLED);

    #elif USE_LIGHT_SLEEP
      #if DEBUG
        Serial.println("light sleep");
      #endif

      // https://github.com/esp8266/Arduino/blob/master/libraries/esp8266/examples/LowPowerDemo/LowPowerDemo.ino
      wifi_fpm_set_sleep_type(LIGHT_SLEEP_T);

      //wifi_fpm_set_wakeup_cb(wakeupCallback); // set wakeup callback
      // the callback is optional, but without it the modem will wake in 10 seconds then delay(10 seconds)
      // with the callback the sleep time is only 10 seconds total, no extra delay() afterward

      wifi_fpm_open();
      wifi_fpm_do_sleep(SLEEP_TIME);  // Sleep range = 10000 ~ 268,435,454 uS (0xFFFFFFE, 2^28-1)
      delay(SLEEP_TIME + 1);          // delay needs to be 1 mS longer than sleep or it only goes into Modem Sleep

    #else
      // Wenn alle sleep modes abgeschaltet sind lassen wir den präprozessor eine Warnung rausschreiben.
      #warning "You do not use any sleep mode!"
      // Und verwenden delay um SLEEP_TIME zu warten
      delay(SLEEP_TIME);
    #endif
  #else
    delay(SLEEP_TIME);
  #endif

  #if DEBUG
    // wird beim aufwachen von deep sleep nicht geloggt, weil es da einen reset gibt
    Serial.println("wake up");
  #endif
}
